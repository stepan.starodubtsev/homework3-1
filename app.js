const number = 2;
console.log(typeof number, '- it`s first answer');

const name = 'Stepan';
const lastname = 'Starodubtsev';
console.log(`I\`m ${name}, ${lastname}`, '- it`s second answer');

console.log(`My number is ${number}`, '- it`s second answer');